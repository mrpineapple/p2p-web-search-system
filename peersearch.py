# "--boot [Integer Identifier 232] --myPort    [PortNo]    #Bootstrap Node Initialization
# "--bootstrap [PortNo] --id [Integer Identifier 232]  --myPort [PortNo]     #Launch Of Other Nodes
# "--commandnode [PortNo] --id [Integer Identifier 232]  --myPort [PortNo]     #Node to test operations

import socket
import threading
import sys
import json
import time

DEBUG = False

IP = "127.0.0.1"
MY_PORT = 0
BOOTSTRAP_PORT = 0

ourword = ""
hashedid = 0
boottype = 0

network = {}
forwards = {}
associated_urls = {}

ackwaiting = []
searched = {}
s_timeout = {}

ping_ack = {}

def hashCode(str):
    hash = 0;
    for x in xrange(len(str)):
        hash = hash * 31 + ord(str[x])
    return abs(hash)

if(sys.argv[1] == "--boot"):
    ourword = sys.argv[2]
    hashedid = hashCode(sys.argv[2])
    boottype = 1
    MY_PORT = sys.argv[4] 
    print "Starting Node Of Network on port " + str(MY_PORT)

if(sys.argv[1] == "--bootstrap"):
    ourword = sys.argv[4]
    BOOTSTRAP_PORT = sys.argv[2]
    hashedid = hashCode(sys.argv[4])
    boottype = 2
    MY_PORT = sys.argv[6]     
    print "Creating New Network Node on port " + str(MY_PORT) + " and attaching to port " + str(BOOTSTRAP_PORT)

if(sys.argv[1] == "--commandnode"):
    ourword = sys.argv[4]
    BOOTSTRAP_PORT = sys.argv[2]
    hashedid = hashCode(sys.argv[4])
    boottype = 3
    MY_PORT = sys.argv[6]     
    print "Creating New Network Command Node on port " + str(MY_PORT) + " and attaching to port " + str(BOOTSTRAP_PORT)

class ListeningThread(threading.Thread):
    def __init__(self,ip,my_port):
        self.ip = ip
        self.my_port = my_port
        threading.Thread.__init__(self)
    
    def run(self):
        recieveMessage(self.ip,self.my_port)

class BroadcastThread(threading.Thread):
    def __init__(self,message,ip,their_port):
        self.message = message
        self.ip = ip
        self.their_port = their_port
        threading.Thread.__init__(self)
    
    def run(self):
        sendMessage(self.message,self.ip,self.their_port)

class PingThread(threading.Thread):
    def __init__(self,word,message,ip,their_port):
        self.message = message
        self.ip = ip
        self.word = word
        self.their_port = their_port
        threading.Thread.__init__(self)
    
    def run(self):
        #wait 30 seconds for a response
        while searched[self.word] == None and time.time() - 30 > s_timeout[self.word]:
            pass
        #if the longer timeout has also failed
        if searched[self.word]:
            #sending PING to node
            sendMessage(self.message,self.ip,self.their_port)
            while ping_ack[self.their_port] == None and time.time() - 40 > s_timeout[self.word]:
                pass
            if ping_ack[self.port] == None:
                msg = json.dumps([{
                    "datatype": "LEAVING_NETWORK",
                    "node_id": hashCode(self.word),
                }], separators=(',',':'))
                sendMessage(msg,self.ip,self.their_port)
            ping_ack = None


def sendMessage(message,ip,their_port):
    #dealing with acknowledgements
    rec = json.loads(message)
    datatype = rec[0]["datatype"]

    if(DEBUG):
        print "sending: ", message , " to port ", their_port
    else:
        print "sending: ", datatype , " to " , their_port
    sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
    sock.sendto(message, (ip, int(their_port)))
    
    if datatype == "INDEX":
        #acknowledgement waiting
        word = rec[0]["target_id"]
        timeout = False         #10 second timeout check
        ack_recieved = False    #Acknowledgement check

        while(not timeout and not ack_recieved):
            ack_check = 0
            for x in xrange(len(ackwaiting)):
                try:
                    #are we still waiting on an Acknowledgement?
                    if ackwaiting[x][0] == word:
                        ack_check = 1
                        #if we are, are we past the timeout allowance?
                        if (time.time() > ackwaiting[x][1] + 30):
                            timeout = False
                            print "ACK not recieved."
                except IndexError:
                    #Try-Catch is to protect against index errors if waiting var is deleted whilst we are browsing the list. Likely.
                    pass
            if ack_check == 0:
                #we couldnt find the acknowledgement in our waiting list.
                #i.e. the request has been acknowledged
                ack_recieved = True
                print "Acknowledged."

    #only the original cares about the search results. others can just ignore
    if datatype == "SEARCH" and rec[0]["sender_port"] == MY_PORT:
        #   wait for our SEARCH RESULT Response and return to function to give back our data 
        while searched[rec[0]["word"]] == None and time.time() < s_timeout[rec[0]["word"]] + 10:
            pass
        
        #so, we didn't get a response, we're gonna ping the node and check if it's still alive
        if searched[rec[0]["word"]] == None:
            message = json.dumps([{
                "datatype": "PING",
                "target_id": rec[0]["node_id"], #suspected dead node.
                "sender_port": MY_PORT,  
                "port": MY_PORT     
            }], separators=(',',':'))
            ping = PingThread(rec[0]["word"],message,ip,their_port)
            ping.start()    


def recieveMessage(ip,port):
    print "waiting to recieve on port " , port
    sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
    sock.bind((ip, int(port) ))

    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        rec = json.loads(data)
        datatype = rec[0]["datatype"]
        print "received message: ", datatype

        if(datatype == "JOINING_NETWORK_SIMPLIFIED"):
            #extract info from json
            node_id = rec[1]["node_id"]
            fromport = rec[3]["port"]

            #replying ROUTING_INFO
            message = json.dumps([{
                "datatype": "ROUTING_INFO",
                "gateway_id": MY_PORT, 
                "node_id": node_id,
                "port": fromport ,
                "route_table":[{    #only this node in the routing table for now
                   "node_id": hashedid,
                   "port": MY_PORT
                }]
            }], separators=(',',':'))
            bthread = BroadcastThread(message,"127.0.0.1",fromport)
            bthread.start()
            bthread.join()

            #forwards the JOINING_NETWORK_RELAY to a closer node if there & relay any routing_infos passed
            for key in network:
                if(abs(hashedid - node_id) > abs(key - node_id)):
                    message = json.dumps([{
                        "datatype": "JOINING_NETWORK_RELAY_SIMPLIFIED", # a string
                        "node_id": node_id, 
                        "target_id": key, 
                        "gateway_id": hashedid,
                    }]
                    , separators=(',',':'))
                    bthread = BroadcastThread(message,"127.0.0.1",network[key])
                    bthread.start()
                    bthread.join()                
            #add to network after broadcast (so we dont check it in the for loop)
            network[node_id] = fromport


        #used for new nodes to populate their routing table
        if(datatype == "ROUTING_INFO"):
            #if this is a response from a network relay, send it to the joined node
            if hashedid == rec[0]["gateway_id"]:
                bthread = BroadcastThread(data,"127.0.0.1",network[rec[0]["node_id"]])
                bthread.start()
                bthread.join()  

            else:
                #as a joining node, if we recieve routing information, add it to our knowledgebase of the network
                route = rec[0]["route_table"]
                for x in xrange(len(route)):
                    network[route[x]["node_id"]] = route[x]["port"]

        if(datatype == "JOINING_NETWORK_RELAY_SIMPLIFIED"):
            #if we recieve a information about a node, send routing_info through the gateway node
            message = json.dumps([
                {
                "datatype": "ROUTING_INFO",
                "gateway_id": rec[0]["gateway_id"], 
                "node_id":  rec[0]["node_id"], 
                "port": MY_PORT,
                "route_table":[{    #attaching all our knowledge
                   "node_id": hashedid,
                   "port": MY_PORT
                }
                ]
            }], separators=(',',':'))
            forwards[rec[0]["node_id"]] = rec[0]["gateway_id"]     #if we dont know the port, we can send it to this node and they'll know
            #send back to gateway 
            bthread = BroadcastThread(message,"127.0.0.1",network[rec[0]["gateway_id"]])
            bthread.start()
            bthread.join()                
        if(datatype == "LEAVING_NETWORK"):
            del network[rec[0]["node_id"]]
            for key in network:
                if abs(key - rec[0]["node_id"]) < abs(hashedid - rec[0]["node_id"]):
                    bthread = BroadcastThread(message,"127.0.0.1",network[rec[0]["gateway_id"]])
                    bthread.start()
                    bthread.join() 

        if(datatype == "INDEX"):
            if(hashedid == rec[0]["target_id"]):
                if(rec[0]["link"][0] in associated_urls ):
                    associated_urls[rec[0]["link"][0]] = associated_urls[rec[0]["link"][0]] + 1
                else:
                    associated_urls[rec[0]["link"][0]] = 1
                print "Indexed."
                #send ACK
                message =  json.dumps([ {
                     "datatype": "ACK",
                     "node_id": hashedid,
                     "port": ourword #seemingly this should be the keyword according to the spec.
                }], separators=(',',':')) 
                bthread = BroadcastThread(message,"127.0.0.1",rec[0]["sender_port"])
                bthread.start()
                bthread.join()
                
            else:
                searchid = rec[0]["target_id"] 
                #We dont have the data, give it to some other node
                if not searchid in network:
                    if searchid in forwards:
                        bthread = BroadcastThread(data,"127.0.0.1",forwards[searchid])
                        bthread.start()
                        bthread.join()

                    else:
                        any_closer = False
                        for key in network:
                            #lets check if a closer one knows how to get there
                            if(abs(key - searchid) < abs(hashedid - searchid)):
                                any_closer = True
                                #forward it to a closer node.
                                bthread = BroadcastThread(data,"127.0.0.1",network[key])
                                bthread.start()
                                bthread.join()
                        if not any_closer :
                            print "Node not found."
                else:
                    bthread = BroadcastThread(data,"127.0.0.1",network[searchid])
                    bthread.start()
                    bthread.join()
        if datatype == "ACK":
            for x in xrange(len(ackwaiting)):
                if ackwaiting[x][0] == rec[0]["node_id"]:
                    del ackwaiting[x]
        if(datatype == "SEARCH"):
            if(hashedid == rec[0]["node_id"]):
                message =  json.dumps([ 
                    {
                    "datatype": "SEARCH_RESPONSE",
                    "word": ourword, 
                    "sender_id": hashedid, 
                    "response": associated_urls
                }], separators=(',',':')) 
                bthread = BroadcastThread(message,"127.0.0.1",rec[0]["sender_port"])
                bthread.start()
                bthread.join()
                
            else:
                searchid = rec[0]["node_id"] 
                #similar to index routing code
                if not searchid in network:
                    if searchid in forwards:
                        bthread = BroadcastThread(data,"127.0.0.1",forwards[searchid])
                        bthread.start()
                        bthread.join()
                    else:
                        any_closer = False
                        for key in network:
                            if(abs(key - searchid) < abs(hashedid - searchid)):
                                any_closer = True
                                bthread = BroadcastThread(data,"127.0.0.1",network[key])
                                bthread.start()
                                bthread.join()
                        if not any_closer :
                            #null response
                            message =  json.dumps([ 
                                {
                                "datatype": "SEARCH_RESPONSE",
                                "word": "null", 
                                "node_id": rec[0]["sender_id"], 
                                "sender_id": hashedid, 
                                "response": [{"null",0}]    #signals a null response
                            }], separators=(',',':')) 
                            bthread = BroadcastThread(message,"127.0.0.1",rec[0]["sender_port"])
                            bthread.start()
                            bthread.join()

                else:
                    bthread = BroadcastThread(data,"127.0.0.1",network[searchid])
                    bthread.start()
                    bthread.join()
        if(datatype == "SEARCH_RESPONSE"):
            searched[rec[0]["word"]] = rec[0]["response"]
        if(datatype == "PING"):      
            #similar overlay routing to search & index routines
            if(hashedid == rec[0]["target_id"]):
                message = json.dumps([{
                    "type": "PING_ACK", 
                    "node_id": hashedid, 
                    "port": port ,
                }
                ], separators=(',',':'))
                bthread = BroadcastThread(data,"127.0.0.1",rec[0]["port"])
                bthread.start()
                bthread.join()
            else:
                searchid = rec[0]["target_id"] 
                #similar to index routing code
                if not searchid in network:
                    if searchid in forwards:
                        bthread = BroadcastThread(data,"127.0.0.1",forwards[searchid])
                        bthread.start()
                        bthread.join()
                    else:
                        any_closer = False
                        for key in network:
                            if(abs(key - searchid) < abs(hashedid - searchid)):
                                any_closer = True
                                bthread = BroadcastThread(data,"127.0.0.1",network[key])
                                bthread.start()
                                bthread.join()
                        if not any_closer :
                            #if we can't find the node, do nothing. We could create a new message type to indicate loss, but thats outside the spec
                            pass


        if(datatype == "PING_ACK"):
            ping_ack["node_id"] = True


def main():

    if boottype == 2:
        #Join an existing network
        joinNetwork(IP,hashedid,BOOTSTRAP_PORT)  
    else:
        if boottype == 3:
            #command node, do what i tell you to :)
            joinNetwork(IP,hashedid,BOOTSTRAP_PORT)  
            indexPage("http://test.com",["dog","cat","dog","dog","mouse","squirrel"])
            indexPage("http://ianhunter.ie",["dog","mouse"])
            search(["dog","mouse","squirrel"])
        else:
            #Boot node - begin listening for events
            lthread = ListeningThread(IP,MY_PORT)  #are we supposed be listening on an ip >_>
            lthread.start()
            lthread.join()

#changing this. See Readme
class SearchResult(object):
    def __init__(self,word,urls):
        self.word = word
        self.urls = urls


##
#
# send JOINING_NETWORK_SIMPLIFIED message to gateway node
# gateway node responds with ROUTING_INFO
#    maybe forward this to a closer node identifier JOINING_NETWORK_RELAY_SIMPLIFIED
#
##
def joinNetwork(bootstrap_node,identifier,target_identifier):
    #Disregarding Target Identifier as I can't make sense of it. Why do we need both the address of the bootstrap node, and also the hashcode?
    print "--Attempting To Join Network--"
    message = json.dumps([{'datatype':'JOINING_NETWORK_SIMPLIFIED'},{'node_id':identifier},{'target_id':target_identifier},{'port':MY_PORT}], separators=(',',':'))
    bthread = BroadcastThread(message,bootstrap_node,BOOTSTRAP_PORT)
    lthread = ListeningThread(bootstrap_node,MY_PORT)
    bthread.start()
    lthread.start()
    bthread.join()
    time.sleep(5)    #we need to include this because the node was sending out requests before it knew anything about the network
#    lthread.join() #we want this to stay up indefinelty


def leaveNetwork(network_id):
    for key in network:
        message = json.dumps([
            {
            "datatype": "LEAVING_NETWORK", 
            "node_id": hashedid,
            }
        ], separators=(',',':'))
        
        bthread = BroadcastThread(message,bootstrap_node,network[key])
        bthread.start()
        bthread.join()
        exit(0)

##
#   says - this URL has these words, please tell the words that they are located here, thanks.
#
#   broadcast INDEX for each word passed
#   when recieved, word-link should be stored in local storage & the count of the node frequency
##
def indexPage(url,unique_words):
    for word in unique_words:
        ackwaiting.append((hashCode(word),time.time()))    # [(value,broadcast_time)]

        if hashedid == hashCode(word):
            if(url in associated_urls ):
                associated_urls[url] = associated_urls[url] + 1
            else:
                associated_urls[url] = 1
            print "Indexed."
            #we can remove it from our timeout list so
            for x in xrange(len(ackwaiting)):
                if ackwaiting[x][0] == hashCode(word):
                    del ackwaiting[x]

        else:
            message = json.dumps([
                {
                    "datatype": "INDEX", 
                    "target_id": hashCode(word),      #the target id
                    "sender_port": MY_PORT,                 ##changed this.  check README
                    "keyword": word,       #the word being indexed
                    "link": [
                        url                #changed this. We aren't passing in multiple addresses
                    ]
                }
            ], separators=(',',':'))
            searchid = hashCode(word)
            
            if not searchid in network:
                if searchid in forwards:
                    bthread = BroadcastThread(message,IP,forwards[searchid])
                    bthread.start()
                    bthread.join()
                else:
                    any_closer = False
                    for key in network:
                        #lets check if a closer one knows how to get there
                        if(abs(key - searchid) < abs(hashedid - searchid)):
                            any_closer = True
                            #forward it to a closer node.
                            bthread = BroadcastThread(message,IP,network[key])
                            bthread.start()
                            bthread.join()
                    if not any_closer :
                        print "Node not found"
                        return None
            else:
                bthread = BroadcastThread(message,IP,network[hashCode(word)])
                bthread.start()
                bthread.join()
                
            time.sleep(3)


##
#
#   SEARCH messages to be sent on the network
#   Recieve SEARCH_RESPONSE messages
#   3 seconds pass to allow results to be composed.
#   aggregate results and return response
#
#   30 second ping check to check if nodes are still alive if they didnt respond
#   no success for 10 seconds - remove node from the network
#
## 
def search(words):
    result = []
    for word in words:
        global searched
        searched[word] = None   #nothing in here to begin with
        s_timeout[word] = time.time()

        message = json.dumps([
            {
                "datatype": "SEARCH", 
                "word": word, 
                "node_id": hashCode(word), 
                "sender_port": MY_PORT,    #changed this 
            }
        ], separators=(',',':'))

        #similar to the indexing code. Route the request to the correct node.
        searchid = hashCode(word)
        if searchid == hashedid:
            print "Search Result - " , word, " , " , associated_urls
            result.append(SearchResult(word,associated_urls))
            continue
        if not searchid in network:
            if searchid in forwards:
                bthread = BroadcastThread(message,IP,forwards[searchid])
                bthread.start()
                bthread.join()
            else:
                any_closer = False
                for key in network:
                    if(abs(key - searchid) < abs(hashedid - searchid)):
                        any_closer = True
                        bthread = BroadcastThread(message,IP,network[key])
                        bthread.start()
                        bthread.join()
                if not any_closer :
                    print "Node not found."
                    return None
        else:
            bthread = BroadcastThread(message,IP,network[hashCode(word)])
            bthread.start()
            bthread.join()
            
        time.sleep(4) 
        result.append(SearchResult(word,searched[word]))

    searched = {}    #reset dictionary so we dont rely on cached results next time
    for x in result:
        print "Results: " ,x.urls
    return result    #by the time we get to here, search results have been evaluated in sendMessage

if __name__ == '__main__':
    main()